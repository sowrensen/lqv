(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Login.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Login.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _repositories_Factory__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../repositories/Factory */ "./resources/js/repositories/Factory.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var settingsRepository = _repositories_Factory__WEBPACK_IMPORTED_MODULE_0__["Factory"].get("settings");
var menuList = [{
  icon: "mdi-inbox",
  label: "Inbox",
  separator: true
}, {
  icon: "mdi-send-circle-outline",
  label: "Outbox",
  separator: false
}, {
  icon: "mdi-delete-outline",
  label: "Trash",
  separator: false
}, {
  icon: "mdi-alert-circle-outline",
  label: "Spam",
  separator: true
}, {
  icon: "mdi-settings-outline",
  label: "Settings",
  separator: false
}, {
  icon: "mdi-message-alert-outline",
  label: "Send Feedback",
  separator: false
}, {
  icon: "mdi-help-circle-outline",
  iconColor: "primary",
  label: "Help",
  separator: false
}];
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      drawer: true,
      menuList: menuList,
      miniState: false,
      appVersion: null
    };
  },
  mounted: function mounted() {
    this.getVersion();
  },
  methods: {
    drawerClick: function drawerClick(e) {
      // if in "mini" state and user
      // click on drawer, we switch it to "normal" mode
      if (this.miniState) {
        this.miniState = false; // notice we have registered an event with capture flag;
        // we need to stop further propagation as this click is
        // intended for switching drawer to "normal" mode only

        e.stopPropagation();
      }
    },
    getVersion: function getVersion() {
      var _this = this;

      settingsRepository.getVersion().then(function (response) {
        return _this.appVersion = response.data.version;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Login.vue?vue&type=template&id=12f5395a&":
/*!***************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Login.vue?vue&type=template&id=12f5395a& ***!
  \***************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "q-layout",
    { attrs: { view: "hHh LpR fFf" } },
    [
      _c(
        "q-header",
        {
          staticClass: "bg-white text-grey-9 q-py-sm",
          attrs: { reveal: "", bordered: "" }
        },
        [
          _c(
            "q-toolbar",
            [
              _c("q-btn", {
                attrs: { dense: "", flat: "", round: "", icon: "mdi-menu" },
                on: {
                  click: function($event) {
                    _vm.drawer = !_vm.drawer
                  }
                }
              }),
              _vm._v(" "),
              _c("q-separator", {
                attrs: { dark: "", vertical: "", inset: "" }
              }),
              _vm._v(" "),
              _c(
                "q-toolbar-title",
                [
                  _c("q-avatar", { staticClass: "q-mx-xs" }, [
                    _c("img", { attrs: { src: "assets/images/wolf-head.png" } })
                  ]),
                  _vm._v("LQV\n        ")
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "q-drawer",
        {
          attrs: {
            mini: !_vm.drawer || _vm.miniState,
            "content-class": "text-grey-8",
            side: "left",
            bordered: "",
            "show-if-above": ""
          },
          on: {
            "!click": function($event) {
              return _vm.drawerClick($event)
            }
          },
          model: {
            value: _vm.drawer,
            callback: function($$v) {
              _vm.drawer = $$v
            },
            expression: "drawer"
          }
        },
        [
          _c(
            "q-scroll-area",
            { staticClass: "fit" },
            _vm._l(_vm.menuList, function(menuItem, index) {
              return _c(
                "q-list",
                { key: index },
                [
                  _c(
                    "q-item",
                    {
                      directives: [{ name: "ripple", rawName: "v-ripple" }],
                      attrs: {
                        clickable: "",
                        active: menuItem.label === "Outbox",
                        "active-class": "bg-deep-purple-5 text-white"
                      }
                    },
                    [
                      _c(
                        "q-item-section",
                        { attrs: { avatar: "" } },
                        [_c("q-icon", { attrs: { name: menuItem.icon } })],
                        1
                      ),
                      _vm._v(" "),
                      _c("q-item-section", [_vm._v(_vm._s(menuItem.label))])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  menuItem.separator ? _c("q-separator") : _vm._e()
                ],
                1
              )
            }),
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "q-mini-drawer-hide absolute",
              staticStyle: { top: "15px", right: "-17px" }
            },
            [
              _c("q-btn", {
                attrs: {
                  dense: "",
                  round: "",
                  unelevated: "",
                  color: "accent",
                  icon: "mdi-arrow-left"
                },
                on: {
                  click: function($event) {
                    _vm.miniState = true
                  }
                }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("q-page-container"),
      _vm._v(" "),
      _c(
        "q-footer",
        { staticClass: "bg-grey-8 text-white", attrs: { bordered: "" } },
        [
          _c("q-toolbar", [
            _c("small", [
              _vm._v(
                "© " +
                  _vm._s(new Date().getFullYear()) +
                  " Sowren Sen | Version: " +
                  _vm._s(_vm.appVersion)
              )
            ])
          ])
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Login.vue":
/*!**************************************!*\
  !*** ./resources/js/views/Login.vue ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Login_vue_vue_type_template_id_12f5395a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Login.vue?vue&type=template&id=12f5395a& */ "./resources/js/views/Login.vue?vue&type=template&id=12f5395a&");
/* harmony import */ var _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Login.vue?vue&type=script&lang=js& */ "./resources/js/views/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Login_vue_vue_type_template_id_12f5395a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Login_vue_vue_type_template_id_12f5395a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Login.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Login.vue?vue&type=script&lang=js&":
/*!***************************************************************!*\
  !*** ./resources/js/views/Login.vue?vue&type=script&lang=js& ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Login.vue?vue&type=template&id=12f5395a&":
/*!*********************************************************************!*\
  !*** ./resources/js/views/Login.vue?vue&type=template&id=12f5395a& ***!
  \*********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_12f5395a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=template&id=12f5395a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Login.vue?vue&type=template&id=12f5395a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_12f5395a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_12f5395a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);