<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quize extends Model
{
    protected $guarded = ['id'];

    public function questions()
    {
        return $this->hasMany('App\Question');
    }
}
