<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = ['id'];

    public function quize()
    {
        return $this->belongsTo('App\Quize');
    }

    public function choices()
    {
        return $this->hasMany('App\Choice');
    }
}
