<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Catamaran:400,700|Source+Sans+Pro:400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    <link rel="stylesheet" href="{{ mix('/css/main.css') }}">
</head>

<body>
    <div id="app" v-cloak>
        {{-- The application loads here. Following tag directs to resources/js/app.js --}}
        <app></app>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
</body>

</html>