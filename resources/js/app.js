/**
 * This file is the event horizon of the application. Nothing outside
 * of this file matters, and once you reached here, you're stuck!
 * 
 * It creates a Vue instance for the app, loads all the packages and styles.
 */
import './bootstrap'
import Vue from 'vue'
import VueRouter from 'vue-router'
import '@mdi/font/css/materialdesignicons.min.css'

/**
 * Plugins to be loaded
 */
import './plugins'

/**
 * Base routing engine
 */
import router from './router'

/**
 * Base vuex store
 */
import store from './store'

/**
 * Base filters
 * NOTE: Filters don't need to be registered
 */
import filters from './filters/filters'

/**
 * Base view 
 */
import App from './views/App'

/**
 * Mixins used in application
 */
import helpers from './mixins/helpers'

/**
 * Global Vue instance
 */
window.Vue = Vue;

/**
 * Plugins used in the application
 */
Vue.use(VueRouter);

/**
 * Registering mixins
 */
Vue.mixin({
    mixins: [helpers]
});

/**
 * Redirect user to login if not authenticated
 */
router.beforeEach((to, from, next) => {
    let requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    if (requiresAuth && !store.getters['auth/isAuthenticated']) {
        console.log('Unauthorized!');
        next('/login');
    } else if (to.path == '/login' && store.getters['auth/isAuthenticated']) {
        next('/');
    } else {
        next();
    }
});

/**
 * The singularity, where everything coincides
 * 
 */
const app = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App),
});
