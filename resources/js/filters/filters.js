import Vue from 'vue'
import moment from 'moment'

Vue.filter('fromNow', value => moment(value).fromNow())
Vue.filter('datetoread', value => value ? moment(value).format('MMM DD, YYYY') : "")
Vue.filter('ucwords', value => _.startCase(_.lowerCase(value)))
Vue.filter('ordinal', value => {
    if (!value) return '';
    let ends = ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th'];
    if (((value % 100) >= 11) && ((value % 100) <= 13)) {
        return `${value}th`;
    } else {
        return `${value}${ends[value%10]}`;
    }
})
Vue.filter('expired', (value, issueExpirationLimit) => {
    return moment(value).add(issueExpirationLimit, 'd');
})
Vue.filter('format', value => {
    if (moment(Date.parse(value)).isValid()) {
        return moment(value).format('MMM DD, YYYY')
    }
    return _.startCase(_.lowerCase(value))
})
