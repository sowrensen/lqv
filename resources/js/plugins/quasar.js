import Vue from 'vue';

import Quasar from 'quasar'
import mdi from 'quasar/icon-set/mdi-v3'

Vue.use(Quasar, {
    iconSet: mdi
    // components: All,
    // directives: All,
    // plugins: All,
    // animations: All
});
