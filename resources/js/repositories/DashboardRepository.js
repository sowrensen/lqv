import Repository from './Repository'

export default {
    getData() {
        return Repository.get('/dashboard');
    }
}
