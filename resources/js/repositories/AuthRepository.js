import Repository from './Repository'

export default {
    enter(credentials) {
        return Repository.post('/login', credentials);
    },
    exit() {
        return Repository.post('/logout');
    },
    register(data) {
        return Repository.post('/register', data);
    }
}
