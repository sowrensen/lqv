import axios from 'axios'

/**
 * The baseURL with api endpoint precedence
 */
const baseURL = '/api';

/**
 * baseURL can be defined here dynamically
 * according to app environment.
 */
// if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {}

/**
 * Default axios instance
 */
let instance = axios.create({
    baseURL,
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/json',
        'Content-type': 'application/json',
    }
});

/**
 * Update api token with each request
 */
instance.interceptors.request.use(config => {
    config.headers.Authorization = `Bearer ${localStorage.getItem('API_TOKEN')}`;
    return config;
}, error => console.error(error));


export default instance;
