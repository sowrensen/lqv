import AuthRepository from './AuthRepository'
import UserRepository from './UserRepository'
import SettingsRepository from './SettingsRepository'
import DashboardRepository from './DashboardRepository'

const repositories = {
    auth: AuthRepository,
    user: UserRepository,
    settings: SettingsRepository,
    dashboard: DashboardRepository,
}

export const Factory = {
    get: name => repositories[name]
};
