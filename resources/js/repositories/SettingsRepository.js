import Repository from "./Repository";

export default {
    getSettings(type = "") {
        return Repository({
            url: '/settings',
            method: 'get',
            params: {
                setType: type
            }
        })
    },
    setSettings(data) {
        return Repository.patch('/settings', data)
    },
    getVersion() {
        return Repository.get('/version')
    }
}
