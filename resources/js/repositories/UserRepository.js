import Repository from './Repository'

export default {
    getUsers(parameters = {}) {
        return Repository({
            url: '/users',
            method: 'get',
            params: parameters
        })
    },
    showUser(id) {
        return Repository.get(`/users/${id}`)
    },
    updateUser(data) {
        return Repository.patch(`/users/${data.id}/edit`, data)
    },
    switchUser(data) {
        return Repository.patch(`users/${data.id}/switch`, data)
    },
    removeUser(id) {
        return Repository.delete(`users/${id}/delete`)
    }
}
