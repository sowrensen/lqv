/**---------------------------------------------------------------
 * Application Routes
 * ---------------------------------------------------------------
 * 
 * This file contains all the available application route. 
 * Here we are using dynamic import to split the app.js file 
 * into several chunks. To know more about dynamic import, please visit: 
 * https: //laravel-news.com/using-dynamic-imports-with-laravel-mix
 * 
 */

import store from '../store'

/**
 * Provide route permit
 * 
 * @param {String} to Route to
 * @param {String} from Route from
 * @param {Function} next Callback function
 */
let getRoutePermit = (to, from, next) => {
    store.getters['auth/isAdmin'] ? next() : next(from);
}

let routes = [{
        path: '/',
        name: 'home',
        alias: '/home',
        component: () => import('../views/Home.vue'),
        redirect: '/dashboard',
        meta: {
            requiresAuth: true
        },
        children: [
            // open access routes
            {
                path: '/dashboard',
                name: 'dashboard',
                component: () => import('../views/Dashboard.vue')
            },
            // closed access routes
            {
                path: '/settings',
                name: 'settings',
                component: () => import('../views/Settings.vue'),
                beforeEnter: getRoutePermit
            },
        ],
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('../views/Login.vue')
    }
]


export default routes
