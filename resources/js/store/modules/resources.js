import {
    Factory
} from "../../repositories/Factory";

const settingsRepository = Factory.get("settings");

/**
 * This is the resources module for store. 
 * Frequently used resources are fetched during login.
 * 
 */

const state = {
    settings: JSON.parse(localStorage.getItem('settings')),
}

const getters = {
    settings: state => state.settings,
}

const actions = {
    // Fetch application settings from api
    fetchSettings: function (context) {
        // 
    }
}


const mutations = {
    setSettings: (state, payload) => state.settings = payload,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
