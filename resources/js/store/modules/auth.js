import {
    Factory
} from "../../repositories/Factory";
const authRepository = Factory.get('auth');
const userRepository = Factory.get('user');
/**
 * This is the auth module for store. 
 * 
 */

// users with admin privilege
const permitted = ["super-admin", "admin"];

// initial state
const state = {
    token: localStorage.getItem('API_TOKEN') || '',
    user: JSON.parse(localStorage.getItem('user')),
    status: null,
}

// getters
const getters = {
    isAuthenticated: state => !!state.token && !!state.user,
    authStatus: state => state.status,
    user: state => state.user,
    isAdmin: state => state.user ? permitted.indexOf(state.user.roles[0].name) != -1 : false,
    reservations: state => state.reservations,
}

// actions
const actions = {
    login: function (context, payload) {
        return new Promise(function (resolve, reject) {
            authRepository.enter(payload).then(response => {
                // Save user data to local storage
                localStorage.setItem('user', JSON.stringify(response.data.payload.user));
                localStorage.setItem('API_TOKEN', response.data.payload.token);

                // Commit changes to the store
                context.commit('setUser', JSON.parse(localStorage.getItem('user')));
                context.commit('setToken', localStorage.getItem('API_TOKEN'));
                context.commit('setStatus', true);

                // Send acknowledgement to view
                resolve(response.data.message);
            }).catch(error => reject(error));
        });
    },
    logout: function (context) {
        return new Promise(function (resolve, reject) {
            authRepository.exit().then(response => {
                // Clear local storage
                localStorage.clear();

                // Commit changes to the store
                context.commit('setUser', null);
                context.commit('setToken', null);

                // Send acknowledgement to view
                resolve(response.data.message);
            }).catch(error => {
                // Clear localstorage anyway if error occures
                localStorage.clear();
                reject(error);
            });
        });
    },
    fetchUser: function (context) {
        userRepository.showUser(state.user.id).then(response => {
            localStorage.setItem('user', JSON.stringify(response.data.payload));
            context.commit('setUser', response.data.payload);
        }).catch(errors => console.log(errors));
    }
}

// mutations
const mutations = {
    setUser: (state, payload) => {
        state.user = payload
    },
    setToken: (state, payload) => {
        state.token = payload
    },
    setStatus: (state, payload) => {
        state.status = payload
    },
}

// exporting auth store module
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
