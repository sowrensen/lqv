/**
 * Main state management module of the application.
 * All modules are assembled and exported here.
 */
import Vue from 'vue'
import Vuex from 'vuex'

// demo import of store modules
import auth from './modules/auth'
import resources from './modules/resources'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        // add imported modules here
        auth,
        resources,
    },
    strict: debug,
})
